# obmp

#### 介绍
    object memory pool，轻量级内存池，为每个thread添加缓冲区，以object为单位进行缓存，在高并发场景下，有效改善glibc ptmalloc2的内存峰值、内存黑洞问题。

#### 软件架构
软件架构说明
    obmp为每个线程分配一个缓冲区chunk，并按照8字节对齐分配多个block，每个block中的内存单元(unit，返还给申请方使用)来自于chunk；申请内存时，通过size找到对应的block，在block中寻找空闲状态的内存单元返还给申请方；管理结构如下：
    ![输入图片说明](https://images.gitee.com/uploads/images/2021/1123/145729_b426766f_7935460.png "屏幕截图.png")

#### 性能数据
1. 配置：Kunpeng-920(aarch64 64*CPU) 操作系统：openEuler-21.03 gcc版本：gcc (GCC) 9.3.1
2. 测试对象：容器管理引擎iSulad(https://gitee.com/openeuler/iSulad)
3. 测试方法: 使用性能测试工具进行高并发(并发640组请求)测试，对比高并发之后内存占用及耗时。
4. 结果展示
- 默认版本

    ![默认版本](https://images.gitee.com/uploads/images/2021/1123/151209_8c9cfb6e_7935460.png "屏幕截图.png")
- 使用obmp

    ![使用obmp](https://images.gitee.com/uploads/images/2021/1123/151357_ad2adf31_7935460.png "屏幕截图.png")
    
    **在使用obmp之后，并发的Create/Start/Stop/Remove操作耗时差异在500ms之内，高并发后内存占用减少40%**

- 各并发数下内存占用对比

    ![输入图片说明](image.png)
#### 安装教程


#### 使用说明

1.  将动态分配内存的地方替换为obmp提供的接口；
2.  编译时将obmp链接到目标程序；

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### TODO
-  支持hook 库函数调用，做到无感链接
-  完善Debug信息
-  能够检查内存问题

#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
