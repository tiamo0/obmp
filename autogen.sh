#!/bin/bash

set -x
autoheader
aclocal
libtoolize
autoheader
autoconf
automake --foreign --add-missing --copy
