/******************************************************************************
 * Copyright (c) KylinSoft  Co., Ltd. 2021. All rights reserved.
 * lcr licensed under the Mulan PSL v2.

 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *     http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v2 for more details.
 * Author: xiapin
 * Create: 2021-12-24
 * Description: provide memory block function define.
 ******************************************************************************/
#ifndef _MEM_BLOCK_H_
#define _MEM_BLOCK_H_

#include "obmp_type.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    S32         alloced;
    S32         used;
} block_stats_t;

void *clib_malloc(size_t size);

void clib_free(void *ptr);

void *clib_calloc(size_t nmemb, size_t size);

void *clib_realloc(void *ptr, size_t size);

void *clib_reallocarray(void *ptr, size_t nmemb, size_t size);

size_t clib_malloc_usable_size(void *ptr);

mem_block_info_t **mem_block_init(U32 block_cnt);

void release_tcache_chunk(struct st_chunk **chunk);

void alloc_tcache_chunk(struct st_chunk **chunk, size_t cache_size);

void *get_mem_from_block(struct st_chunk *chunk, mem_block_info_t *mem_block, size_t size);

void *get_mem_from_clib(size_t size);

S32 get_block_status(void *base_addr, mem_block_info_t *blk, block_stats_t *blk_stat);

#ifdef __cplusplus
};
#endif
#endif
