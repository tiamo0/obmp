/******************************************************************************
 * Copyright (c) KylinSoft  Co., Ltd. 2021. All rights reserved.
 * lcr licensed under the Mulan PSL v2.

 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *     http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v2 for more details.
 * Author: xiapin
 * Create: 2021-12-24
 * Description: provide memory pool funcion declare.
 ******************************************************************************/
#ifndef _OBMP_H_
#define _OBMP_H_

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
    MP_OPT_USED_MAIN_AREA       = 0,
} MP_OPT;

typedef long unsigned int size_t;

extern void *obmp_malloc(size_t size);

extern void obmp_free(void *ptr);

extern void *obmp_calloc(size_t nmemb, size_t size);

extern void *obmp_strdup(const char *s);

extern void *obmp_strndup(const char *s, size_t n);

extern int obmp_set(MP_OPT opt, void *value);

#ifdef __cplusplus
};
#endif

#endif
